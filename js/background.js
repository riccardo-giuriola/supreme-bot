function timerStart(time){
  var today = new Date();
  var cur_hour = today.getHours();
  var cur_minute = today.getMinutes();
  //var cur_time = today.getHours() + ":" + today.getMinutes();
  if(parseInt(cur_hour) < 10){
    cur_hour = "0" + cur_hour;
  }
  if(parseInt(cur_minute) < 10){
    cur_minute = "0" + cur_minute;
  }
  var cur_time = cur_hour + ":" + cur_minute;
  console.log(cur_time);
  if(cur_time != time){
    return false;
  }
  return true;
}

chrome.runtime.onMessage.addListener(function(message, sender) {

      if(!message.timerStart) return;

      var checkTimer = setInterval(function(){
        chrome.storage.sync.get(['time', "item_category", "item_name"], function(options){
            var today = new Date();
            var cur_time = today.getHours() + ":" + today.getMinutes();
            if(timerStart(options.time)){

              if(options.item_category == null){
                alert("Please insert the item category");
                clearInterval(checkTimer);
              }
              else{

                chrome.cookies.set({
                  url: "https://www.supremenewyork.com/",
                  name: "running",
                  value: "true"
                });

                var newURL = "https://supremenewyork.com/shop/all/" + options.item_category;
                chrome.tabs.create({ url: newURL });

                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    // query the active tab, which will be only one tab
                    //and inject the script in it
                    chrome.tabs.executeScript(tabs[0].id, {file: "content_script.js"});
                });
                clearInterval(checkTimer);
              }

            }
          });
      }, 1000);
});
