var cookies = document.cookie;
var cookie = cookies.split(";");
var status = "false";
for(var i = 0; i < cookie.length; i++){
  if(cookie[i].split("=")[0] == "running" || cookie[i].split("=")[0] == " running"){
    status = cookie[i].split("=")[1];
  }
}

if(status == "true"){
  chrome.storage.sync.get(['status', 'item_name', 'item_category', 'item_color', 'cloth_size', 'shoe_size', 'name', 'email', 'phone', 'address', 'postcode', 'city', 'country', 'card_type', 'card_number', 'expire_month', 'expire_year', 'cvv'], function(options){

    var addToBasket = false;
    var item_found = false;

      var checkItemName = setInterval(function(){
        //click the desired item
        var items = document.getElementsByTagName("a");
        for(i = 0; i < items.length; i++){
          if(options.item_name == "Undefined" || options.item_name == "" || options.item_name == null){

          }
          else if (options.item_color == "Undefined" || options.item_color == "" || options.item_color == null){
            if (items[i].text == options.item_name){
              items[i].click();
              item_found = true;
              clearInterval(checkItemName);
            }
          }
          else if (items[i].text == options.item_name && items[i+1].text == options.item_color){
            items[i].click();
            item_found = true;
            clearInterval(checkItemName);
          }
        }
        if(item_found == false){
          if(window.location == ("https://www.supremenewyork.com/shop/all/" + options.item_category)){
            location.reload();
          }
        }
      }, 500);

    var checkSize = setInterval(function(){
      //select the desired size id="size"
      var size = document.getElementsByName("size");
      for(i = 0; i < size.length; i++){
        for(j = 0; j < size[i].options.length; j++){
          if (size[i].options[j].text == options.cloth_size || (size[i].options[j].text).includes(options.shoe_size)){
            for(k = 0; k < size[i].options.length; k++){
              size[i].options[k].selected = false;
            }
            size[i].options[j].selected = true;
            clearInterval(size);
          }
        }
      }
    }, 500);

    //.getAttribute('itemprop')

    var checkAddToBasket = setInterval(function(){
      //click on add to basket name="commit"
        var basket = document.getElementsByName("commit");
        for(i = 0; i < basket.length; i++){
          if (basket[i].value == "add to basket"){
            addToBasket = true;
            basket[i].click();
            clearInterval(checkAddToBasket);
          }
          else if(basket[i].value == "remove"){
            addToBasket = true;
          }
        }
    }, 500);


    var checkCheckoutNow = setInterval(function(){
      // //click on checkout now
      if(addToBasket){
        var checkout_now = document.getElementsByTagName("a");
        for(i = 0; i < checkout_now.length; i++){
          if (checkout_now[i].text == "checkout now"){
            checkout_now[i].click();
            clearInterval(checkCheckoutNow);
          }
        }
      }
    }, 1000);

    //if(window.location == "https://supremenewyork.com/checkout"){
      var checkForm = setInterval(function(){
        var billing_name = document.getElementsByName("order[billing_name]")[0];
        if(typeof(billing_name) != 'undefined' && billing_name != null){
          //fill the billing name
          billing_name.value = options.name;
          //fill the email
          document.getElementsByName("order[email]")[0].value = options.email;
          //fill the user phone
          document.getElementById("order_tel").value = options.phone;
          //fill the billing address
          document.getElementsByName("order[billing_address]")[0].value = options.address;
          //fill the zip code
          document.getElementsByName("order[billing_zip]")[0].value = options.postcode;
          //fill the billing city
          document.getElementById("order_billing_city").value = options.city;

          //select the billing country
          var billing_country = document.getElementsByName("order[billing_country]");
          for(i = 0; i < billing_country.length; i++){
            for(j = 0; j < billing_country[i].options.length; j++){
              if (billing_country[i].options[j].text == options.country){
                for(k = 0; k < billing_country[i].options.length; k++){
                  billing_country[i].options[k].selected = false;
                }
                billing_country[i].options[j].selected = true;
              }
            }
          }

          //select the card type name="credit_card[type]"
          var card_type = document.getElementsByName("credit_card[type]");
          for(i = 0; i < card_type.length; i++){
            for(j = 0; j < card_type[i].options.length; j++){
              if (card_type[i].options[j].text == options.card_type){
                for(k = 0; k < card_type[i].options.length; k++){
                  card_type[i].options[k].selected = false;
                }
                card_type[i].options[j].selected = true;
              }
            }
          }

          //fill the card number name="credit_card[cnb]"
          document.getElementsByName("credit_card[cnb]")[0].value = options.card_number;
          //fill the expire month name="credit_card[month]"
          var expire_month = document.getElementsByName("credit_card[month]");
          for(i = 0; i < expire_month.length; i++){
            for(j = 0; j < expire_month[i].options.length; j++){
              if (expire_month[i].options[j].text == options.expire_month){
                for(k = 0; k < expire_month[i].options.length; k++){
                  expire_month[i].options[k].selected = false;
                }
                expire_month[i].options[j].selected = true;
              }
            }
          }

          //fill the expire year name="credit_card[year]"
          var expire_year = document.getElementsByName("credit_card[year]");
          for(i = 0; i < expire_year.length; i++){
            for(j = 0; j < expire_year[i].options.length; j++){
              if (expire_year[i].options[j].text == options.expire_year){
                for(k = 0; k < expire_year[i].options.length; k++){
                  expire_year[i].options[k].selected = false;
                }
                expire_year[i].options[j].selected = true;
              }
            }
          }

          //fill the cvv code name="credit_card[ovv]" id="vval"
          document.getElementsByName("credit_card[ovv]")[0].value = options.cvv;

          //accept the terms
          document.getElementsByTagName("ins")[1].click();

          clearInterval(checkForm);
        }
      }, 100);
    //}

    //buy the item value="process payment"
    var items = document.getElementsByTagName("input");
    for(i = 0; i < items.length; i++){
      if (items[i].value == "process payment"){
        items[i].click();
      }
    }

    // var isRecaptchaFrame = () => {
    //   return /https:\/\/www.google.com\/recaptcha\/api2\/anchor/.test(window.location.href);
    // };

    // var isRecaptchaImageFrame = () => {
    //   return /https:\/\/www.google.com\/recaptcha\/api2\/bframe/.test(window.location.href);
    // };

    //var captchaInterval = setInterval(function() {
      // if (isRecaptchaFrame()) {
      //   document.getElementsByClassName('recaptcha-checkbox-checkmark')[0].click();
      // }
    //   if(isRecaptchaImageFrame()){
    //     alert("frame");
    //     var audio_button_check = setInterval(function(){
    //       var audio_button = document.getElementById('recaptcha-audio-button');
    //       if(typeof(audio_button) != 'undefined' && audio_button != null){
    //         audio_button.click();
    //         clearInterval(audio_button_check);
    //       }
    //     }, 500);
    //
    //     var audio_input_check = setInterval(function(){
    //       var audio_input = document.getElementById('audio-response');
    //       if(typeof(audio_input) != 'undefined' && audio_input != null){
    //         audio_input.value = 'prova';
    //         clearInterval(audio_input_check);
    //       }
    //     }, 500);
    //
    //     clearInterval(captchaInterval);
    //   }
    // }, 500);

  });
}
