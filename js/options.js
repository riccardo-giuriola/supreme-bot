window.onload = function(){

  chrome.storage.sync.get(['name','email','phone','address','postcode','city','country','card_type','card_number','expire_month','expire_year','cvv'], function(options){
    document.getElementById("name").value = options.name;
    document.getElementById("email").value = options.email;
    document.getElementById("phone").value = options.phone;
    document.getElementById("address").value = options.address;
    document.getElementById("postcode").value = options.postcode;
    document.getElementById("city").value = options.city;
    document.getElementById("country").value = options.country;
    document.getElementById("card_type").value = options.card_type;
    document.getElementById("card_number").value = options.card_number;
    document.getElementById("expire_month").value = options.expire_month;
    document.getElementById("expire_year").value = options.expire_year;
    document.getElementById("cvv").value = options.cvv;
  });
}
$(function(){

  $('#save').click(function(){
    var name = $('#name').val();
    var email = $('#email').val();
    var phone = $('#phone').val();
    var address = $('#address').val();
    var postcode = $('#postcode').val();
    var city = $('#city').val();
    var country = $('#country').val();
    var card_type = $('#card_type').val();
    var card_number = $('#card_number').val();
    var expire_month = $('#expire_month').val();
    var expire_year = $('#expire_year').val();
    var cvv = $('#cvv').val();
    chrome.storage.sync.set({'name': name,
                             'email': email,
                             'phone': phone,
                             'address': address,
                             'postcode': postcode,
                             'city': city,
                             'country': country,
                             'card_type': card_type,
                             'card_number': card_number,
                             'expire_month': expire_month,
                             'expire_year': expire_year,
                             'cvv': cvv}, function(){
      close();
    });
  });

  $('#reset').click(function(){
    chrome.storage.sync.set({'name': '',
                             'email': '',
                             'phone': '',
                             'address': '',
                             'postcode': '',
                             'city': '',
                             'country': '',
                             'card_type': '',
                             'card_number': '',
                             'expire_month': '',
                             'expire_year': '',
                             'cvv': ''});
  })
});
