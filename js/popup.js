const homepage = `
<div style="width: 100%; height: 100px; background-color: #212838;">
  <img src="img/cyberbot_logo.png" height="80" alt="cyberbot logo" style="display: block; margin: 0 auto; padding-top: 15px;">
</div>
<div class="container" style="margin-top: -20px;">
  <div class="row">
    <div class="col-sm">
      <label style="color:white; margin-bottom:-20px; font-size:14px;">Item Name</label>
      <input id="item_name" id="item_name" type="text" class="form-control" style="margin-top:10px;">
    </div>
    <div class="col-sm" style="width:200px;">
      <label style="color:white; margin-top:10px; margin-bottom:-20px; font-size:14px;">Cloth Size</label>
      <select id="cloth_size" class="form-control" style="margin-top:10px;">
        <option selected disabled>--Select--</option>
        <option value="Small">Small</option>
        <option value="Medium">Medium</option>
        <option value="Large">Large</option>
        <option value="XLarge">XLarge</option>
      </select>
    </div>
    <div class="col-sm" style="width:200px;">
      <label style="color:white; margin-top:10px; margin-bottom:-20px; font-size:14px;">Shoe Size</label>
      <select id="shoe_size" class="form-control" style="margin-top:10px;">
        <option selected disabled>--Select--</option>
        <option value="8">8</option>
        <option value="8.5">8.5</option>
        <option value="9">9</option>
        <option value="9.5">9.5</option>
        <option value="10">10</option>
        <option value="10.5">10.5</option>
        <option value="11">11</option>
        <option value="11.5">11.5</option>
        <option value="12">12</option>
        <option value="12.5">12.5</option>
        <option value="13">13</option>
      </select>
    </div>
    <div class="col-sm">
      <label style="color:white; margin-top:10px; margin-bottom:-20px; font-size:14px;">Item Color</label>
      <input id="item_color" type="text" placeholder="Item color" class="form-control" style="margin-top:10px;">
    </div>
    <div class="col-sm">
      <label style="color:white; margin-top:10px; margin-bottom:-20px; font-size:14px;">Item Category</label>
      <select id="item_category" class="form-control" style="margin-top:10px;">
        <option selected disabled>--Select--</option>
        <option value="jackets">Jackets</option>
        <option value="shirts">Shirts</option>
        <option value="shoes">Shoes</option>
        <option value="t-shirts">T-Shirts</option>
        <option value="tops_sweaters">Tops_sweaters</option>
        <option value="sweatshirts">Sweatshirts</option>
        <option value="pants">Pants</option>
        <option value="shorts">Shorts</option>
        <option value="hats">Hats</option>
        <option value="bags">Bags</option>
        <option value="accessories">Accessories</option>
        <option value="skates">Skate</option>
      </select>
    </div>
    <div class="col-sm">
      <label style="color:white; margin-top:10px; margin-bottom:-20px; font-size:14px;">Start time</label>
      <input id="time" type="time" placeholder="Item name" class="form-control" style="margin-top:10px;">
    </div>
  </div>
  <!--<div id="status" style="color:#23BF2D; float:right; margin-top:10px;"></div>-->
</div>
<div style="width: 100%; height: 60px; margin-top: 30px; background-color: #07142b;">
  <div style="padding-top:4px;">
    <button id="shutdown" type="button" class="btn btn-icon" style="height: 30px; float:right; margin-top:7px; margin-right:4px; border-radius:4px; font-size:12px;"><i class="fas fa-power-off fa-2x"></i></button>
    <button id="reset" type="button" class="btn btn-icon" style="height: 30px; float:right; margin-top:7px; border-radius:4px; font-size:12px;"><i class="fas fa-trash-alt fa-2x"></i></button>
    <button id="options" type="button" class="btn btn-icon" style="height: 30px; float:right; margin-top:7px; border-radius:4px; font-size:12px;"><i class="fas fa-cog fa-2x"></i></button>
    <button id="start" type="button" class="btn btn-primary" style="width: 58%; height: 30px; float:right; margin-top:10px; margin-right:0px; border-radius:4px; font-size:12px;">Save / Start</button>
  </div>
</div>
`;

const login = `
<div style="width: 100%; height: 100px; background-color: #212838;">
  <img src="img/cyberbot_logo.png" height="80" alt="cyberbot logo" style="display: block; margin: 0 auto; padding-top: 15px;">
</div>
  <div class="container">
    <div class="row" style="margin-top: -20px;">
      <div class="col-sm">
        <label style="color:white; margin-bottom:-20px; font-size:14px;">Email</label>
        <input id="email" type="text" class="form-control" style="margin-top:10px;">
      </div>
      <div class="col-sm">
        <label style="color:white; margin-bottom:-20px; font-size:14px;">Password</label>
        <input id="password" type="password" class="form-control" style="margin-top:10px;">
      </div>
    </div>
    <div id="status" style="color:#23BF2D; float:right; margin-top:10px;"></div>
    <div style="margin-top:15px;">
      <button id="login" type="button" class="btn btn-icon" style="background-color: #db2c39; border-color: #db2c39 !important; float:right; margin-top:10px; height: 30px; border-radius:4px; font-size:12px;">Login</button>
      <button id="forgot_password" type="button" class="btn btn-icon" style="background-color: #db2c39; border-color: #db2c39 !important; float:right; margin-top:10px; margin-right:5px; height: 30px; border-radius:4px; font-size:12px;">Forgot Password</button>
    </div>
  </div>
`;

function ajaxCall(script, email, password, callback) {

  var dataString = 'email=' + email + '&password=' + password;
  // AJAX code to submit form.
  $.ajax({
    type: "POST",
    url: "localhost/php/" + script,
    data: dataString,
    cache: false,
    success: function(result) {
      callback(result);
    },
    error: function (errorThrown) {
            callback(errorThrown);
        }
  });
}

function loadValues(){
  chrome.storage.sync.get(['cloth_size', 'shoe_size', 'item_name', 'item_color','item_category','time'], function(options){
    document.getElementById("item_name").value = options.item_name;
    document.getElementById("cloth_size").value = options.cloth_size;
    document.getElementById("shoe_size").value = options.shoe_size;
    document.getElementById("item_color").value = options.item_color;
    document.getElementById("item_category").value = options.item_category;
    document.getElementById("time").value = options.time;
  });
}

$(function(){

  var body = document.getElementById("body");

  ajaxCall("classes/Login.php", null, null, function(data){
    console.log(data);
    if(data == 1){
      body.style.width = "400px";
      body.style.height = "500px";
      body.innerHTML = homepage;
      loadValues();
    }
    else{
      body.style.width = "400px";
      body.style.height = "300px";
      body.innerHTML = login;
    }

    $('#login').click(function(){
      ajaxCall("login.php", $('#email').val(), $('#password').val(), function(data){
        console.log(data);
        if(data == 1){
          body.style.width = "400px";
          body.style.height = "550px";
          body.innerHTML = homepage;
          loadValues();
        }
        else{
          document.getElementById("email").value = "";
          document.getElementById("password").value = "";
          var status = document.getElementById("status");
          status.style.color = '#FF0000';
          status.innerHTML = "Incorrent Credentials.";
        }
      });
    })

    $('#start').bind("click", function(e) {
      chrome.runtime.sendMessage({'timerStart': true});

      var item_name = $('#item_name').val().replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
      var cloth_size = $('#cloth_size').val();
      var shoe_size = $('#shoe_size').val();
      var item_color = $('#item_color').val().replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
      var item_category = $('#item_category').val();
      var time = $('#time').val();
      chrome.storage.sync.set({'cloth_size': cloth_size,
                               'shoe_size': shoe_size,
                               'item_name': item_name,
                               'item_color': item_color,
                               'item_category': item_category,
                               'time': time}, function(){
      });
    });

    $('#options').click(function(){
      chrome.tabs.create({ url: "options.html" });
    })

    $('#reset').click(function(){
      chrome.storage.sync.set({'size': '',
                               'item_name': '',
                               'cloth_size': '',
                               'shoe_size': '',
                               'item_color': '',
                               'item_category': '',
                               'time': ''});
      loadValues();
    });

    $('#shutdown').click(function(){
      chrome.cookies.set({
        url: "https://www.supremenewyork.com/",
        name: "running",
        value: "false"
      });
    })

  });
});
